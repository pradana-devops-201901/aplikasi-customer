package com.pradana.training.devops.aplikasicustomer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplikasiCustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplikasiCustomerApplication.class, args);
	}

}
