package com.pradana.training.devops.aplikasicustomer.dao;

import com.pradana.training.devops.aplikasicustomer.entity.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CustomerDao extends PagingAndSortingRepository<Customer, String> {
}
