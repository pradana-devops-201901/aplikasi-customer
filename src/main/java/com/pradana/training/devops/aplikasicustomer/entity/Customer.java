package com.pradana.training.devops.aplikasicustomer.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity @Data
public class Customer {
    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotEmpty @Size(min = 3, max = 255)
    private String fullname;

    @Email
    @NotEmpty @Size(min = 6, max = 100)
    private String email;

    @NotEmpty @Size(min = 5, max = 20)
    private String mobilePhone;
}
