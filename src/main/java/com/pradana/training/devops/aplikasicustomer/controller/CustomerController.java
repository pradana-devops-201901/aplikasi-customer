package com.pradana.training.devops.aplikasicustomer.controller;

import com.pradana.training.devops.aplikasicustomer.dao.CustomerDao;
import com.pradana.training.devops.aplikasicustomer.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class CustomerController {
    @Autowired
    private CustomerDao customerDao;

    @GetMapping("/customer/")
    public Page<Customer> lihatDataCustomer(Pageable page) {
        return customerDao.findAll(page);
    }

    @GetMapping("/customer/{id}")
    public Customer cariById(@PathVariable(name = "id") Customer c) {
        return c;
    }

    @PostMapping("/customer/")
    public void simpanDataCustomer(@RequestBody @Valid Customer c) {
        customerDao.save(c);
    }
}
